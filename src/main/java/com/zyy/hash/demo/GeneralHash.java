package com.zyy.hash.demo;

import java.util.Arrays;

/**
 * @author zyy
 * @date 2021/1/12 下午5:45
 * @description 普通HASH算法的实现
 * 普通Hash算法存在一个问题，以ip_hash为例，假定下载用户ip固定没有发生改变，
 * 现在tomcat3出现 了问题，down机了，服务器数量由3个变为了2个，之前所有的
 * 求模都需要重新计算。
 *
 * 如果在真实生产情况下，后台服务器很多台，客户端也有很多，那么影响是很大的，缩容和扩容都会存
 * 在这样的问题，大量用户的请求会被路由到其他的目标服务器处理，用户在原来服务器中的会话都会丢
 * 失。
 *
 */
public class GeneralHash {
    public static void main(String[] args) {

        //定义客户端IP
        String[] clients = new String[]{"10.78.12.3","113.25.63.1","126.12.3.8"};
        //定义服务器数量
        int serverCount = 5;

        //hash(ip) % node_counts = index
        //根据index锁定应该路由到的tomcat服务器
        for (String client : clients) {
            int hash = Math.abs(client.hashCode());
            int index = hash % serverCount;
            System.out.println("客户端：" + client + " 被路由到的服务器为："+ index);
        }

    }
}
