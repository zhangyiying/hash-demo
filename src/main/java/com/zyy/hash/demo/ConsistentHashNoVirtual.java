package com.zyy.hash.demo;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author zyy
 * @date 2021/1/12 下午6:03
 * @description 一致性Hash算法
 *
 *
 *首先有一条直线，直线开头和结尾分别定为为1和2的32次方减1，这相当于一个地址，
 *对于这样一条 线，弯过来构成一个圆环形成闭环，这样的一个圆环称为hash环。
 * 我们把服务器的ip或者主机名求 hash值然后对应到hash环上，那么针对客户端用户，
 * 也根据它的ip进行hash求值，对应到环上某个位 置，然后如何确定一个客户端路由到
 * 哪个服务器处理呢?按照顺时针方向找最近的服务器节点
 *
 * 假如将服务器3下线，服务器3下线后，原来路由到3的客户端重新路由到服务器4，
 * 对于其他客户端没有 影响只是这一小部分受影响(请求的迁移达到了最小，
 * 这样的算法对分布式集群来说非常合适的，避免 了大量请求迁移 )
 *
 * 增加服务器5之后，原来路由到3的部分客户端路由到新增服务器5上，
 * 对于其他客户端没有影响只是这 一小部分受影响(请求的迁移达到了最小，
 * 这样的算法对分布式集群来说非常合适的，避免了大量请求 迁移 )
 *
 */
public class ConsistentHashNoVirtual {

    public static void main(String[] args) {
        //step1 初始化：把服务器ip的哈希值对应到哈希环上
        //定义服务器IP
        String[] tomcatServers = new String[]{"123.111.0.0","123.101.3.1","111.20.35.2","123.98.26.3"};

        SortedMap<Integer,String> hashServerMap =  new TreeMap<>();

        for (String tomcatServer : tomcatServers) {
            //求出每一个Ip对应的 Hash值
            int serverHash = Math.abs(tomcatServer.hashCode());
            //存储Hash值和IP的对应关系
            hashServerMap.put(serverHash,tomcatServer);
        }

        //step2 针对客户端IP求出hash值
        // 定义客户端IP
        String[] clients = new String[]{"10.78.12.3","113.25.63.1","126.12.3.8"};
        for (String client : clients) {
            int clientHash = Math.abs(client.hashCode());
            //step3 针对客户端 找到能够处理当前客户端请求的服务器（hash环上顺时针最近）
            //根据客户端ip的hash值去找出哪一个服务器节点能够处理
            SortedMap<Integer, String> integerStringSortedMap = hashServerMap.tailMap(clientHash);
            if(integerStringSortedMap.isEmpty()){
                //取hash环上顺时针第一条数据
                Integer firstKey = hashServerMap.firstKey();
                System.out.println("==========>>>>客户端：" + client + " 被路由到服务器：" + hashServerMap.get(firstKey));
            }else {
                Integer firstKey = integerStringSortedMap.firstKey();
                System.out.println("==========>>>>客户端：" + client + " 被路由到服务器：" + hashServerMap.get(firstKey));
            }

        }

    }

}
